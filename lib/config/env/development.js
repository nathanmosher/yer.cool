'use strict';

module.exports = {
  env: 'development',
  mongo: {
    uri: process.env.IP || 'mongodb://localhost/fullstack-dev'
  }
};